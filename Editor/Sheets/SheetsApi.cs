using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GoogleSheetsDownloader.Data;
using GoogleSheetsDownloader.Google;
using GoogleSheetsDownloader.Utility;
using UnityEngine;

namespace GoogleSheetsDownloader.Sheets
{
	[Serializable]
	public class SheetMetaData
	{
		public string SheetName;
		public List<string> Sheets;
	}

	public class Sheet
	{
		public readonly string Name;
		public readonly IList<IList<object>> Values;

		public Sheet(ValueRange valueRange)
		{
			this.Name = valueRange.Range.Substring(0, valueRange.Range.IndexOf('!'));
			this.Values = valueRange.Values;
		}
	}

	public class SheetsApi
	{
		private readonly SheetsService sheetsService;
		private readonly DataObjectJsonFormatter dataObjectJsonFormatter;
		private readonly SheetsApiConfig config;

		public SheetsApi(Task<GoogleCredential> pendingCredential, SheetsApiConfig config = null)
		{
			this.config = config ?? new SheetsApiConfig();

			try {
				this.sheetsService = new SheetsService(pendingCredential);
				this.dataObjectJsonFormatter = new DataObjectJsonFormatter(this.config);
			} catch (Exception ex) {
				Debug.LogException(ex);
				throw;
			}
		}

		public SheetsApi(GoogleCredential credential, SheetsApiConfig config = null)
		{
			this.config = config ?? new SheetsApiConfig();

			try {
				this.sheetsService = new SheetsService(credential);
				this.dataObjectJsonFormatter = new DataObjectJsonFormatter(this.config);
			} catch (Exception ex) {
				Debug.LogException(ex);
				throw;
			}
		}

		public async Task<string> GetAllSheetsAsJson(string spreadSheetId)
		{
			using (DisposableTimer.Measure("Get All Sheets as Json", this.config.Silent)) {
				var metaData = await this.GetSheetMetaData(spreadSheetId);
				var result = await this.GetSheets(spreadSheetId, metaData.Sheets);
				return await SheetsToJson(result);
			}
		}

		public async Task GetAllSheetsAndCreateFiles(string spreadSheetId, string outDir)
		{
			try {
				using (DisposableTimer.Measure("Get All Sheets and Create Json", this.config.Silent)) {
					var metaData = await this.GetSheetMetaData(spreadSheetId);
					var result = await this.GetSheets(spreadSheetId, metaData.Sheets);

					await WriteSheetsToFiles(outDir, result);
				}
			} catch (Exception ex) {
				Debug.LogException(ex);
			}
		}

		public async Task<string> GetSheetAsJson(string spreadSheetId, string sheet)
		{
			using (DisposableTimer.Measure("Get Sheet as Json", this.config.Silent)) {
				var result = await this.GetSheets(spreadSheetId, new[] { sheet });
				return await SheetsToJson(result);
			}
		}

		public async Task<string> GetSheetsAsJson(string spreadSheetId, IList<string> sheets)
		{
			using (DisposableTimer.Measure("Get Sheet as Json", this.config.Silent)) {
				var result = await this.GetSheets(spreadSheetId, sheets);
				return await SheetsToJson(result);
			}
		}


		public Task GetSheetAndCreateFile(string spreadSheetId, string sheet, string outDir)
		{
			return this.GetSheetsAndCreateFiles(spreadSheetId, new[] {
				sheet
			}, outDir);
		}

		public async Task GetSheetsAndCreateFiles(string spreadSheetId, IList<string> sheets, string outDir)
		{
			using (DisposableTimer.Measure("Get Sheets and Create Json", this.config.Silent)) {
				var result = await this.GetSheets(spreadSheetId, sheets);
				await WriteSheetsToFiles(outDir, result);
			}
		}

		private Task WriteSheetsToFiles(string outDir, IList<Sheet> result)
		{
			return Task.Run(() => {
				using (DisposableTimer.Measure("Process Sheets", this.config.Silent)) {
					Directory.CreateDirectory(outDir);

					foreach (var s in result) {
						if (s.Name.Contains("//")) {
							continue;
						}

						var dataObject = SheetParser.ParseSheet(s);
						var container = new DataObject(DataObjectType.Object);
						container.Set(s.Name, dataObject);

						var str = this.dataObjectJsonFormatter.ToJson(container);

						var fileName = $"{s.Name}.json";
						File.WriteAllText(Path.Combine(outDir, fileName), str);
					}
				}
			});
		}

		private Task<string> SheetsToJson(IList<Sheet> result)
		{
			return Task.Run(() => {
				using (DisposableTimer.Measure("Process Sheets", this.config.Silent)) {
					var output = new DataObject(DataObjectType.Object);
					foreach (var s in result) {
						if (s.Name.Contains("//")) {
							continue;
						}

						var dataObject = SheetParser.ParseSheet(s);
						output.Set(s.Name, dataObject);
					}

					return this.dataObjectJsonFormatter.ToJson(output);
				}
			});
		}

		public async Task<SheetMetaData> GetSheetMetaData(string spreadSheetId)
		{
			using (DisposableTimer.Measure("List Sheets", this.config.Silent)) {
				var result = await this.sheetsService.GetMetadataAsync(spreadSheetId);

				SheetMetaData sheetMetaData = new SheetMetaData();
				sheetMetaData.SheetName = result.Properties.Title;
				sheetMetaData.Sheets = result.Sheets
					.Where(s => !s.Properties.Title.Contains("//"))
					.Select(s => s.Properties.Title)
					.ToList();

				return sheetMetaData;
			}
		}

		public async Task<IList<Sheet>> GetSheets(string spreadSheetId, IList<string> sheets)
		{
			using (DisposableTimer.Measure("Get Sheets " + sheets.Count, this.config.Silent)) {
				try {
					var result = await this.sheetsService.GetSpreadsheetsAsync(spreadSheetId, sheets);
					return result.ValueRanges.Select(s => new Sheet(s)).ToList();
				} catch (Exception e) {
					Debug.LogException(e);
					throw;
				}
			}
		}
	}
}