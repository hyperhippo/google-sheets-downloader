using GoogleSheetsDownloader.Data;

namespace GoogleSheetsDownloader.Sheets
{
	public class SheetsApiConfig
	{
		public string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss";
		public PrintMode PrintMode = PrintMode.Pretty;
		public bool Silent = true;
	}
}