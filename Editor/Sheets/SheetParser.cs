using System;
using System.Collections.Generic;
using System.Linq;
using GoogleSheetsDownloader.Data;
using UnityEngine;

namespace GoogleSheetsDownloader.Sheets
{
	public static class SheetParser
	{
		public static DataObject ParseSheet(Sheet sheet)
		{
			var values = sheet.Values;
			try {
				if ((string)values[0][0] == "// Single Row") {
					if ((string)values[0][1] == "#Key" && (string)values[0][2] == "Value#") {
						return ParseKvp(values);
					}

					var result = ParseObjectSheet(values);

					if (result.Type == DataObjectType.Array) {
						var output = (DataObject)result.Get(0);
						return output;
					}
				}

				return ParseObjectSheet(values);
			} catch (Exception e) {
				Debug.LogError($"Failed to parse sheet: [{sheet.Name}]");
				Debug.LogException(e);
				throw;
			}
		}

		private static List<PropertyPath> ParseHeaderPaths(IList<object> headers)
		{
			try {
				List<PropertyPath> paths = new List<PropertyPath>();
				for (var index = 0; index < headers.Count; index++) {
					var header = headers[index] as string;
					if (!string.IsNullOrWhiteSpace(header) && !header.Contains("//")) {
						paths.Add(new PropertyPath(header, index));
					}
				}

				var endingIndex = 0;

				var clonedPaths = paths.Select(v => new PropertyPath(v)).ToArray();
				var previousPath = clonedPaths[0];

				for (var index = 1; index < paths.Count; index++) {
					var currentPath = clonedPaths[index];
					if (currentPath.KeysAreSame(previousPath)) {
						endingIndex++;
						paths[index].Key[currentPath.Key.Length - 1] = new PathKey(endingIndex);
						paths[index - 1].Key[currentPath.Key.Length - 1] = new PathKey(endingIndex - 1);
					} else {
						endingIndex = 0;
						previousPath = currentPath;
					}
				}

				return paths;
			} catch (Exception e) {
				Debug.LogException(e);
				throw;
			}
		}

		private static DataObject ParseObjectSheet(IList<IList<object>> sheet)
		{
			var paths = ParseHeaderPaths(sheet[0]);

			var objects = new List<DataObject>();
			var currentObject = new DataObject(DataObjectType.Object);

			var index = 1;

			for (var i = 1; i < sheet.Count; i++) {
				var row = sheet[i];

				if (row == null || row.Count == 0 || IsComment(row[0])) {
					continue;
				}

				var firstPath = paths[0];
				var firstColumnIsPrimaryIndex = firstPath.Key.Length == 1;
				var isNotBlank = IsBlank(row[firstPath.ColumnIndex]) == false;

				// If the first path is a list then we're assuming we want to return a simple array instead of a list of lists.
				if (firstColumnIsPrimaryIndex && isNotBlank) {
					if (currentObject.Object.Count > 0) {
						objects.Add(currentObject);
					}

					currentObject = new DataObject(DataObjectType.Object);
					index = i;
				}

				ParseRow(paths, row, currentObject, i - index);
			}

			if (currentObject.Object.Count > 0) {
				if (objects.Count == 0 || objects[objects.Count - 1] != currentObject && currentObject.Object.Count > 0) {
					objects.Add(currentObject);
				}
			}

			var dataObject = new DataObject(DataObjectType.Array);
			for (var i = 0; i < objects.Count; i++) {
				DataObject o = objects[i];
				dataObject.Set(i, o);
			}

			return dataObject;
		}

		private static void ParseRow(List<PropertyPath> paths, IList<object> row, DataObject obj, int index)
		{
			foreach (PropertyPath path in paths) {
				var currPath = path.Key.Select(v => v.IsArrayIndex ? new PathKey(index) : v).ToArray();
				if (path.ColumnIndex > row.Count - 1) {
					continue;
				}

				var value = row[path.ColumnIndex];
				if (!IsBlank(value)) {
					obj.Set(currPath, GetValue(value));
				}
			}
		}

		private static DataObject ParseKvp(IList<IList<object>> sheet)
		{
			var obj = new DataObject(DataObjectType.Object);

			for (var i = 1; i < sheet.Count; i++) {
				var row = sheet[i];

				if (IsComment(row[1])) {
					continue;
				}

				var value = row[2];

				obj.Set(row[1] as string, value);
			}

			return obj;
		}

		private static object GetValue(object val)
		{
			return val;
		}

		private static bool IsComment(object val)
		{
			if (val is string str) {
				return str.StartsWith("//");
			}

			return false;
		}

		private static bool IsBlank(object val)
		{
			if (val == null) {
				return true;
			}

			if (val is bool || val is double) {
				return false;
			}

			if (val is string str) {
				return string.IsNullOrWhiteSpace(str);
			}

			return false;
		}
	}
}