using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace GoogleSheetsDownloader.Utility
{
	public class DisposableTimer : IDisposable
	{
		private readonly string name;
		private readonly Stopwatch stopWatch;
		private readonly bool silent;

		public DisposableTimer(string name, bool silent = false)
		{
			this.name = name;
			this.stopWatch = Stopwatch.StartNew();
			this.silent = silent;
		}

		public void Dispose()
		{
			if (this.silent == false) {
				Debug.Log($"Task: [{this.name}] completed in {this.stopWatch.Elapsed.TotalMilliseconds}");
			}
		}

		public static DisposableTimer Measure(string name, bool silent = false)
		{
			return new DisposableTimer(name, silent);
		}
	}
}