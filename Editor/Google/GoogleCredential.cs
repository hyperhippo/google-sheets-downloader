using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace GoogleSheetsDownloader.Google
{
	public class GoogleCredential
	{
		private HttpClient http;
		private ActiveToken activeToken;
		private DateTime approximateActiveTokenExpiry;
		private OauthRefreshToken refreshToken;
		private SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);

		public GoogleCredential(HttpClient http, OauthRefreshToken refreshToken)
		{
			this.http = http;
			this.refreshToken = refreshToken;
		}

		public async Task<string> GetAccessToken()
		{
			await this.semaphore.WaitAsync();

			try {
				if (this.activeToken != null && this.approximateActiveTokenExpiry >= DateTime.UtcNow) {
					return this.activeToken.AccessToken;
				}

				var result = await this.http.SendAsync(new HttpRequestMessage(HttpMethod.Post, "https://oauth2.googleapis.com/token") {
					Content = new FormUrlEncodedContent(
						new Dictionary<string, string> {
							{ "client_id", this.refreshToken.ClientId },
							{ "client_secret", this.refreshToken.ClientSecret },
							{ "refresh_token", this.refreshToken.RefreshToken },
							{ "grant_type", "refresh_token" }
						}
					)
				});

				var resultBody = await result.Content.ReadAsStringAsync();

				if (result.StatusCode != HttpStatusCode.OK) {
					Debug.LogError($"Unable to get access token.  Error [{result.StatusCode}]: [{resultBody}]");
					return null;
				}

				this.activeToken = JsonConvert.DeserializeObject<ActiveToken>(resultBody);
				this.approximateActiveTokenExpiry = DateTime.UtcNow + TimeSpan.FromSeconds(this.activeToken.ExpiresIn - 300);
				return this.activeToken.AccessToken;
			} finally {
				this.semaphore.Release();
			}
		}

#pragma warning disable 0649
		internal class ActiveToken
		{
			[JsonProperty("access_token")] public string AccessToken;
			[JsonProperty("expires_in")] public int ExpiresIn;
		}
#pragma warning restore 0649
	}
}