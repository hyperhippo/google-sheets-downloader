namespace GoogleSheetsDownloader.Google
{
	public class OauthRefreshToken
	{
		public string ClientId;
		public string ClientSecret;
		public string RefreshToken;
	}
}