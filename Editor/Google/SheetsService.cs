using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace GoogleSheetsDownloader.Google
{
	public class SheetsService
	{
		private const string SERVICE_URL = "https://sheets.googleapis.com/v4/spreadsheets";
		private GoogleCredential cred;
		private HttpClient http;
		private Task<GoogleCredential> pendingCred;

		public SheetsService(GoogleCredential cred)
		{
			this.cred = cred;
			this.http = new HttpClient();
		}

		public SheetsService(Task<GoogleCredential> pendingCred)
		{
			this.pendingCred = pendingCred;
			this.http = new HttpClient();
		}

		public async Task<SpreadsheetMetadata> GetMetadataAsync(string spreadsheetId)
		{
			var request = new HttpRequestMessage(HttpMethod.Get, SERVICE_URL + "/" + spreadsheetId);
			await this.SetDefaultHeadersAsync(request);

			using (var response = await this.http.SendAsync(request)) {
				var responseBody = await response.Content.ReadAsStringAsync();

				if (response.StatusCode != HttpStatusCode.OK) {
					Debug.LogError($"Error [{(int)response.StatusCode}] sending request: [{responseBody}");
					return null;
				}

				return JsonConvert.DeserializeObject<SpreadsheetMetadata>(responseBody);
			}
		}

		public async Task<Spreadsheet> GetSpreadsheetsAsync(string spreadsheetId, IList<string> sheetIds)
		{
			if (sheetIds.Count == 0) {
				Debug.LogError("You must send at least one sheet id.");
				return null;
			}

			var url = new StringBuilder(SERVICE_URL)
				.Append("/").Append(spreadsheetId)
				.Append("/values:batchGet?ranges=").Append(sheetIds[0]);

			for (int i = 1; i < sheetIds.Count; i++) {
				url.Append("&ranges=").Append(sheetIds[i]);
			}

			url.Append("&valueRenderOption=UNFORMATTED_VALUE");

			var request = new HttpRequestMessage(HttpMethod.Get, url.ToString());
			await this.SetDefaultHeadersAsync(request);

			using (var response = await this.http.SendAsync(request)) {
				var responseBody = await response.Content.ReadAsStringAsync();

				if (response.StatusCode != HttpStatusCode.OK) {
					Debug.LogError($"Error [{(int)response.StatusCode}] sending request: [{responseBody}");
					return null;
				}

				return JsonConvert.DeserializeObject<Spreadsheet>(responseBody);
			}
		}

		private async Task SetDefaultHeadersAsync(HttpRequestMessage request)
		{
			if (this.cred == null && this.pendingCred != null) {
				this.cred = await this.pendingCred;
			}

			request.Headers.Add(HttpRequestHeader.Authorization.ToString(), "Bearer " + await this.cred.GetAccessToken());
			request.Headers.Add(HttpRequestHeader.Accept.ToString(), "application/json");
		}
	}
}