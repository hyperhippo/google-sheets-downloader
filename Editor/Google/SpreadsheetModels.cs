using System.Collections.Generic;
using Newtonsoft.Json;

namespace GoogleSheetsDownloader.Google
{
	public class SpreadsheetMetadata
	{
		[JsonProperty("spreadsheetId")] public string SpreadsheetId;
		[JsonProperty("properties")] public SpreadsheetProperties Properties;
		[JsonProperty("sheets")] public SheetMetadata[] Sheets;
	}

	public class SpreadsheetProperties
	{
		[JsonProperty("title")] public string Title;
	}

	public class SheetMetadata
	{
		[JsonProperty("properties")] public SheetProperties Properties;
	}

	public class SheetProperties
	{
		[JsonProperty("title")] public string Title;
	}

	public class Spreadsheet
	{
		[JsonProperty("spreadsheetId")] public string SpreadsheetId;
		[JsonProperty("valueRanges")] public IList<ValueRange> ValueRanges;
	}

	public class ValueRange
	{
		[JsonProperty("range")] public string Range;
		[JsonProperty("values")] public IList<IList<object>> Values;
	}
}