using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

namespace GoogleSheetsDownloader.Google
{
	public static class GoogleCredentialFactory
	{
		private const string AUTHENTICATION_URL = "https://accounts.google.com/o/oauth2/v2/auth";
		private const string VALIDATION_URL = "https://oauth2.googleapis.com/token";
		private const string REDIRECT_URI = "http://127.0.0.1:{0}/";
		private const string SCOPE = "https://www.googleapis.com/auth/spreadsheets.readonly";
		private const string RESPONSE_TYPE = "code";
		private const string GRANT_TYPE = "authorization_code";

		private static HttpListener listener;
		private static SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);

		public static async Task<GoogleCredential> FromUserAuth(string clientId, string clientSecret, int listenPort = 8000)
		{
			var refreshToken = await GetRefreshTokenFromUserAuth(clientId, clientSecret, listenPort);

			if (refreshToken == null) {
				return null;
			}

			var oauthRefreshToken = new OauthRefreshToken {
				ClientId = clientId,
				ClientSecret = clientSecret,
				RefreshToken = refreshToken
			};

			return new GoogleCredential(new HttpClient(), oauthRefreshToken);
		}

		public static void Logout(string clientId)
		{
			if (listener != null && listener.IsListening) {
				listener.Close();
				listener = null;
			}

			Debug.Log($"Logging current user out for client [{clientId}]");
			EditorPrefs.DeleteKey(EditorPrefKey(clientId));
		}

		private static async Task<string> GetRefreshTokenFromUserAuth(string clientId, string clientSecret, int listenPort)
		{
			await semaphore.WaitAsync();
			try {
				var editorPrefKey = EditorPrefKey(clientId);
				var refreshToken = EditorPrefs.GetString(editorPrefKey, null);

				if (string.IsNullOrEmpty(refreshToken)) {
					var authorizationCode = await GetAuthorizationCode(clientId, listenPort);
					refreshToken = await GetRefreshTokenFromAuthorizationCode(clientId, clientSecret, authorizationCode, listenPort);
					EditorPrefs.SetString(editorPrefKey, refreshToken);
				}

				return refreshToken;
			} finally {
				semaphore.Release();
			}
		}

		private static string EditorPrefKey(string clientId)
		{
			return clientId + ".RefeshToken";
		}

		private static async Task<string> GetAuthorizationCode(string clientId, int listenPort)
		{
			if (listener != null && listener.IsListening) {
				listener.Close();

				// Sleep a tick so the previous listener has time to die and clean up.
				await Task.Delay(1);
			}

			var redirectUri = string.Format(REDIRECT_URI, listenPort);
			var uri = new StringBuilder(AUTHENTICATION_URL, 10)
				.Append("?scope=").Append(Uri.EscapeUriString(SCOPE))
				.Append("&response_type=").Append(Uri.EscapeUriString(RESPONSE_TYPE))
				.Append("&redirect_uri=").Append(Uri.EscapeUriString(redirectUri))
				.Append("&client_id=").Append(Uri.EscapeUriString(clientId));

			Application.OpenURL(uri.ToString());

			using (listener = new HttpListener()) {
				listener.Prefixes.Add(redirectUri);
				listener.Start();

				HttpListenerContext context;

				try {
					context = await listener.GetContextAsync();
				} catch (ObjectDisposedException) {
					return null;
				}

				HttpListenerRequest request = context.Request;

				var authorizationCode = request.QueryString.Get("code");

				var output = new StringBuilder("<html><body>Response: ")
					.Append(authorizationCode == null ? $"Error [{request.QueryString.Get("error")}]" : "Success")
					.Append("<br /> You can now close this window</body></html>");

				var buffer = Encoding.UTF8.GetBytes(output.ToString());
				var response = context.Response;
				response.ContentLength64 = buffer.Length;
				response.OutputStream.Write(buffer, 0, buffer.Length);
				response.OutputStream.Close();

				return authorizationCode;
			}
		}

		private static async Task<string> GetRefreshTokenFromAuthorizationCode(string clientId, string clientSecret, string authorizationCode, int port)
		{
			if (string.IsNullOrEmpty(authorizationCode)) {
				Debug.LogError("Authorization code cannot be empy.");
				return null;
			}

			var redirectUri = string.Format(REDIRECT_URI, port);
			var body = new Dictionary<string, string> {
				{ "code", authorizationCode },
				{ "client_id", clientId },
				{ "client_secret", clientSecret },
				{ "grant_type", GRANT_TYPE },
				{ "redirect_uri", redirectUri }
			};

			var request = new HttpRequestMessage(HttpMethod.Post, VALIDATION_URL) {
				Content = new FormUrlEncodedContent(body)
			};

			var http = new HttpClient();
			var result = await http.SendAsync(request);
			var resultBody = await result.Content.ReadAsStringAsync();

			if (result.StatusCode != HttpStatusCode.OK) {
				Debug.LogError($"Unable to get refresh token.  Error [{result.StatusCode}]: [{resultBody}]");
				return null;
			}

			return JsonConvert.DeserializeObject<AuthorizationCodeResponse>(resultBody).RefreshToken;
		}

#pragma warning disable 0649
		internal class AuthorizationCodeResponse
		{
			[JsonProperty("refresh_token")] public string RefreshToken;
		}
#pragma warning restore 0649
	}
}