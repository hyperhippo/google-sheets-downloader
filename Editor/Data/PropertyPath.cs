using System.Linq;

namespace GoogleSheetsDownloader.Data
{
	public class PropertyPath
	{
		public readonly PathKey[] Key;
		public readonly int ColumnIndex;

		public PropertyPath(string header, int columnIndex)
		{
			this.Key = PathKey.ParseHeader(header);
			this.ColumnIndex = columnIndex;
		}

		public PropertyPath(PropertyPath other)
		{
			this.Key = other.Key.Select(v => new PathKey(v.Key)).ToArray();
			this.ColumnIndex = other.ColumnIndex;
		}

		public bool KeysAreSame(PropertyPath other)
		{
			if (this.Key.Length != other.Key.Length) {
				return false;
			}

			for (int i = 0; i < this.Key.Length; i++) {
				if (Equals(this.Key[i], other.Key[i]) == false) {
					return false;
				}
			}

			return true;
		}
	}
}