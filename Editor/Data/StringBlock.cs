using System.Text;

namespace GoogleSheetsDownloader.Data
{
	public enum PrintMode
	{
		Pretty,
		Condensed
	}

	/// <summary>
	///     Provides an easy to use API for generating complicated strings like JSON.
	/// </summary>
	public class StringBlock
	{
		private enum Mode
		{
			LineStart,
			LineMid
		};

		private readonly StringBuilder text = new StringBuilder(1024);
		private readonly PrintMode printMode;

		private string indentText;
		private int indentLevel;
		private Mode mode;

		public StringBlock(PrintMode indentMode = PrintMode.Pretty)
		{
			this.printMode = indentMode;
			this.indentText = "";
			this.indentLevel = 0;

			this.mode = Mode.LineStart;
		}


		private void AppendIndent()
		{
			if (this.printMode == PrintMode.Pretty) {
				this.text.Append(this.indentText);
			}
		}

		public override string ToString()
		{
			return this.text.ToString();
		}

		public void Append(string text)
		{
			if (this.mode == Mode.LineStart) {
				this.AppendIndent();
			}

			this.text.Append(text);
			this.mode = Mode.LineMid;
		}

		public void AppendLine(string text)
		{
			if (this.mode == Mode.LineStart) {
				this.AppendIndent();
			}

			if (this.printMode == PrintMode.Pretty) {
				this.text.AppendLine(text);
			} else {
				this.text.Append(text);
			}

			this.mode = Mode.LineStart;
		}

		public void NewLine()
		{
			if (this.printMode == PrintMode.Pretty) {
				this.text.AppendLine();
				this.mode = Mode.LineStart;
			}
		}

		public void OpenBlock(string text)
		{
			this.AppendLine(text);
			this.InBrace();
		}

		public void CloseBlock()
		{
			this.OutBrace();
		}

		public void InBrace()
		{
			this.AppendLine("{");
			this.Indent();
		}

		public void OutBrace()
		{
			this.Outdent();
			this.NewLine();
			this.Append("}");
		}

		public void InBracket()
		{
			this.AppendLine("[");
			this.Indent();
		}

		public void OutBracket()
		{
			this.Outdent();
			this.NewLine();
			this.Append("]");
		}

		public void Indent()
		{
			this.SetIndent(this.indentLevel + 1);
		}

		public void Outdent()
		{
			this.SetIndent(this.indentLevel - 1);
		}

		public void SetIndent(int level)
		{
			this.indentLevel = level;
			if (this.printMode == PrintMode.Pretty) {
				this.indentText = new string('\t', this.indentLevel);
			}
		}
	}
}