using System.Collections.Generic;
using System.Linq;

namespace GoogleSheetsDownloader.Data
{
	public enum DataObjectType
	{
		Array,
		Object
	}


	/// <summary>
	///     A JSON like object that can easily add values at a given path.
	/// </summary>
	public class DataObject
	{
		public readonly Dictionary<PathKey, object> Object = new Dictionary<PathKey, object>();

		public readonly DataObjectType Type;

		public DataObject(DataObjectType type)
		{
			this.Type = type;
		}

		public override string ToString()
		{
			return this.Type.ToString();
		}

		public void Set(string path, object obj)
		{
			var keys = path.Split('.').Select(x => new PathKey(x)).ToArray();
			this.Set(keys, obj);
		}

		public void Set(int index, object obj)
		{
			var keys = new[] {
				new PathKey(index)
			};
			this.Set(keys, obj);
		}

		public void Set(PathKey[] path, object obj)
		{
			DataObject currentObject = this;

			var lastIndex = path.Length - 1;
			for (int i = 0; i < lastIndex; i++) {
				PathKey current = path[i];

				if (!currentObject.Object.TryGetValue(current, out var dataObj)) {
					PathKey nextKey = path[i + 1];
					DataObject newObj = new DataObject(nextKey.IsNumber ? DataObjectType.Array : DataObjectType.Object);
					currentObject.Object.Add(current, newObj);
					currentObject = newObj;
				} else {
					currentObject = dataObj as DataObject;
				}
			}

			currentObject.Object[path[lastIndex]] = obj;
		}

		public object Get(string path)
		{
			var keys = path.Split('.').Select(x => new PathKey(x)).ToArray();
			return this.Get(keys);
		}

		public object Get(int path)
		{
			return Get(new[] { new PathKey(path) });
		}

		public object Get(PathKey[] path)
		{
			DataObject currentObject = this;

			var lastIndex = path.Length - 1;
			for (int i = 0; i < lastIndex; i++) {
				PathKey current = path[i];
				if (!currentObject.Object.TryGetValue(current, out var obj)) {
					return null;
				}

				currentObject = obj as DataObject;
			}

			return currentObject.Object[path[lastIndex]];
		}

		public void Delete(string path)
		{
			var keys = path.Split('.').Select(x => new PathKey(x)).ToArray();
			this.Delete(keys);
		}

		public void Delete(PathKey[] path)
		{
			DataObject currentObject = this;

			var lastIndex = path.Length - 1;
			for (int i = 0; i < lastIndex; i++) {
				PathKey current = path[i];

				if (!currentObject.Object.TryGetValue(current, out var dataObj)) {
					return;
				}

				currentObject = dataObj as DataObject;
			}

			currentObject.Object.Remove(path[lastIndex]);
		}
	}
}