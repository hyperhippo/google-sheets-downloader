using System;
using System.Globalization;
using System.Linq;
using GoogleSheetsDownloader.Sheets;
using Newtonsoft.Json;

namespace GoogleSheetsDownloader.Data
{
	public class DataObjectJsonFormatter
	{
		private string dateTimeFormat;
		private PrintMode printMode;

		public DataObjectJsonFormatter(SheetsApiConfig config)
		{
			this.dateTimeFormat = config.DateTimeFormat;
			this.printMode = config.PrintMode;
		}

		public string ToJson(DataObject dataObject)
		{
			StringBlock sb = new StringBlock(this.printMode);

			ToJson(dataObject, sb);

			return sb.ToString();
		}

		public void ToJson(DataObject dataObject, StringBlock sb)
		{
			if (dataObject.Type == DataObjectType.Array) {
				var keys = dataObject.Object.Keys.ToArray();
				Array.Sort(keys);

				sb.InBracket();

				var end = keys.Length;
				for (var i = 0; i < end; i++) {
					var obj = dataObject.Object[keys[i]];
					ToJson(obj, sb);
					if (i < end - 1) {
						sb.AppendLine(",");
					}
				}

				sb.OutBracket();
			} else {
				var keys = dataObject.Object.Keys.ToArray();

				sb.InBrace();

				var end = keys.Length;
				for (var i = 0; i < end; i++) {
					var obj = dataObject.Object[keys[i]];

					if (this.printMode == PrintMode.Pretty) {
						sb.Append($"\"{keys[i]}\": ");
					} else {
						sb.Append($"\"{keys[i]}\":");
					}

					ToJson(obj, sb);
					if (i < end - 1) {
						sb.AppendLine(",");
					}
				}

				sb.OutBrace();
			}
		}

		public void ToJson(object obj, StringBlock sb)
		{
			if (obj is DataObject dataObject) {
				this.ToJson(dataObject, sb);
			} else if (obj is double doubleValue) {
				sb.Append(doubleValue.ToString(CultureInfo.InvariantCulture));
			} else if (obj is float floatValue) {
				sb.Append(floatValue.ToString(CultureInfo.InvariantCulture));
			} else if (obj is long longValue) {
				sb.Append(longValue.ToString());
			} else if (obj is int intValue) {
				sb.Append(intValue.ToString());
			} else if (obj is bool boolValue) {
				sb.Append(boolValue ? "true" : "false");
			} else if (obj is string str) {
				sb.Append(JsonConvert.ToString(str.Trim()));
			} else if (obj is DateTime dateTime) {
				sb.Append("\"" + dateTime.ToString(this.dateTimeFormat) + "\"");
			} else {
				sb.Append("\"" + obj + "\"");
			}
		}
	}
}