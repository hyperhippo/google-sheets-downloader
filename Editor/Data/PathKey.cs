using System;
using System.Linq;

namespace GoogleSheetsDownloader.Data
{
	public class PathKey : IComparable<PathKey>
	{
		public readonly int Index;
		public readonly string Key;

		public readonly bool IsNumber;
		public readonly bool IsArrayIndex;

		private readonly int hashCode;

		public PathKey(string key)
		{
			this.Index = -1;
			this.Key = key;
			this.hashCode = key.GetHashCode();
			this.IsArrayIndex = key == "#";
		}

		public PathKey(int key)
		{
			this.Index = key;
			this.hashCode = key;
			this.IsNumber = true;
		}

		public PathKey(PathKey pathKey)
		{
			this.Index = pathKey.Index;
			this.Key = pathKey.Key;
			this.hashCode = pathKey.hashCode;
			this.IsArrayIndex = this.Key == "#";
			this.IsNumber = pathKey.IsNumber;
		}

		public static PathKey[] ParseHeader(string header)
		{
			// ':' and '.' indicate an object property 
			header = header.Replace(':', '.');

			// '|' indicates an array
			// We replace '|' with '.#.' to make it easy to update the PathKey with proper indices later
			// in the SheetParser
			header = header.Replace("|", ".#.");

			// TODO: Should this just be `header = header.Trim('.')`?
			if (header.EndsWith(".")) {
				header = header.Substring(0, header.Length - 1);
			}

			// At this point we've ensured all separators are '.'
			return header.Split('.').Select(x => new PathKey(x)).ToArray();
		}

		public override string ToString()
		{
			return this.Key ?? this.Index.ToString();
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.hashCode == ((PathKey)obj).hashCode;
		}

		public override int GetHashCode()
		{
			return this.hashCode;
		}

		public int CompareTo(PathKey other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			if (this.IsNumber) {
				return this.Index.CompareTo(other.Index);
			}

			return this.Key.CompareTo(other.Key);
		}
	}
}