# Google Sheet Downloader
A library that downloads Google Sheet documents as JSON.

This library uses Google's OAuth2 APIs to verify who you are an what sheets you have access to, and the Google Sheets APIs to download data as CSV.  It then converts that data to JSON.

## Requirements
+ Unity 2018.4 or later
+ Google Cloud Project with Sheets API and OAuth credentials.

## Usage
```csharp
public async GetData() {
    const string SPREADSHEET_ID = "Your spreadsheet Id";
    const string CLIENT_ID = "OAuth Client ID";
    const string CLIENT_SECRET = "OAuth Client Secret";

    // Authorize your game to access data on your behalf.
    var cred = await GoogleCredentialFactory.FromUserAuth(CLIENT_ID, CLIENT_SECRET);

    // Use the credentials to download all the sheets on the Spreadsheet.
    var api = new SheetsApi(cred);
	var json = await api1.GetAllSheetsAsJson(SPREADSHEET_ID);
}
```


## Syntax
The library supports creating more complex JSON objects through a custom syntax.

### Standard
By default all rows in a sheet will be downloaded as distinct objects in JSON.

![Complex Object](Documentation~/SimpleObject.png)

```json
[
    {
        "Id": 1,
        "X": "Hello",
        "Y": true,
        "Z": 34.99
    },
    {
        "Id": 2,
        "X": "World",
        "Y": false,
        "Z": 9.99
    }
]
```

### Complex Objects
Use dots (`.`) to create complex objects

![Complex Object](Documentation~/ComplexObject.png)

```json
[
    {
        "Id": 1,
        "XX": {
            "Y": "Hello",
            "Z": true,
        }
    },
    {
        "Id": 2,
        "XX": {
            "Y": "World",
            "Z": false,
        }
    }
]
```


### Arrays
End your colunn name with a pipe (`|`) to indicate it's a list of data.  Leave the first column empty to indicate when a row is a continuation of the previous one.

![Arrays](Documentation~/Array.png)

```json
[
    {
        "Id": 1,
        "XX": ["Hello", "World"]
    },
    {
        "Id": 2,
        "XX": ["Lorem","Ipsum"]
    }
]
```

### Complex arrays
Use pipe (`|`) to create complex arrays.  Note that the blank id indicates the second row is a continuation of the first.  

_**NOTE:** You cannot have arrays of arrays._

![Complex Arrays](Documentation~/ComplexArray.png)

```json
[
    {
        "Id": 1,
        "XX": [
            {
                "Y": "Hello",
                "Z": true
            },
            {
                "Y": "World",
                "Z": true,
            }
        ]
    }
]
```

### Comments
You can mark rows, columns, and sheets as ignored by starting them with a double-slash (`//`).

_**Note:** Commenting out the first column may yield unusual results._

![Comments](Documentation~/Comment.png)                        |

```json
[
    {
        "Id": 1,
        "Name" "Hello"
    },
    {
        "Id": 3,
        "Name" "Lorem"
    }
]
```

## Spreadsheet Id
The Spreadsheet ID can be found in the URL for your spreadsheet:

![Spreadsheet Id](Documentation~/GoogleSheetId.png)


## Configuring Google Cloud
A Google Cloud project with OAuth and Sheets API enabled is required for this to work.  Google provides details instructions all over the place, such as https://developers.google.com/identity/protocols/OAuth2InstalledApp.  A quick summary of what's required:

1. Google API Project
    + A Google API project is required to support OAuth authentication.  If you do not have a Google API project you can create one at the [API Console](https://console.developers.google.com/project)
1. Google Sheets API
    + You must enable the Google Sheets API to get the proper scope.  From  "APIs & Services":
        + Click on "Dashboard"
        + Click on "Enable APIs And Services"
        + Search for "Google Sheets API", click on it
        + Click "Enable"
1. OAuth Consent Screen
    + Google OAuth requires you to configure a consent screen.  From "APIs & Services":
        + Click on "OAuth consent screen"
        + Choose "Internal" if you have a GSuite account, otherwise "External".
        + Fill out the form
            + If you don't have a website for this app, enter "example.com" for authorized domains.
        + Add "https://www.googleapis.com/auth/spreadsheets.readonly" to the list of scopes.
1. OAuth Credentials
    + OAuth is used to verify that you are able to access the documents.  From https://console.developers.google.com/apis/credentials
        + Click "Create Credentials"
        + Choose "Oauth client ID"
        + Choose "Other"
        + Fill in a name and click "Create"
        + Note down the "Client ID" and "Client Secret" values.